#
PROG = mrbc
CC = gcc
OBJS =	mrbc.o src/gc.o src/y.tab.o src/state.o src/dump.o src/print.o src/class.o src/variable.o \
				src/array.o src/hash.o src/string.o src/error.o src/pool.o src/codegen.o src/vm.o \
				src/symbol.o src/init.o src/debug.o src/numeric.o src/crc.o src/backtrace.o src/object.o \
				src/proc.o src/etc.o src/kernel.o src/range.o src/compar.o src/enum.o src/version.o 

#HEADERFILES = include/mruby/compile.h 
HEADERFILES =

#ソースの場所
VPATH = . 
#インクルードファイルの場所
#CPPFLAGS = -Wall -g -O2 -Wl,--no-flag-mismatch-warnings -fsigned-char
CPPFLAGS = -Wall -g -O2 
CCINC =  -I ./include 

.PHONY: all
all: $(PROG)

$(PROG): $(OBJS)
	$(CC) $(CPPFLAGS) -o $@ $^

%.o: %.c $(HEADERFILES)
	$(CC) $(CPPFLAGS) $(CCINC) -c $< -o $@
	
.PHONY: clean
clean:
	$(RM) $(PROG) $(OBJS)
